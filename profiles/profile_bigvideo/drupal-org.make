; profile_bigvideo make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.7"
projects[ctools][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.6"
projects[features][subdir] = "contrib"

projects[features_site_frontpage][version] = "1.0"
projects[features_site_frontpage][subdir] = "contrib"

projects[features_extra][version] = "1.0-beta1"
projects[features_extra][subdir] = "contrib"

projects[smart_trim][version] = "1.5"
projects[smart_trim][subdir] = "contrib"

projects[videojs][version] = "3.0-alpha2"
projects[videojs][subdir] = "contrib"

projects[node_export][version] = "3.0"
projects[node_export][subdir] = "contrib"

projects[nodequeue][version] = "2.0"
projects[nodequeue][subdir] = "contrib"

projects[bigvideo][version] = "1.0"
projects[bigvideo][subdir] = "contrib"

projects[block_class][version] = "2.1"
projects[block_class][subdir] = "contrib"

projects[boxes][version] = "1.2"
projects[boxes][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[logo_block][version] = "1.6"
projects[logo_block][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[owlcarousel][version] = "1.4"
projects[owlcarousel][subdir] = "contrib"

projects[features][version] = "2.6"
projects[features][subdir] = "contrib"

projects[features_extra][version] = "1.0-beta1"
projects[features_extra][subdir] = "contrib"

projects[uuid][version] = "1.0-alpha6"
projects[uuid][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha2"
projects[jquery_update][subdir] = "contrib"

projects[variable][version] = "2.5"
projects[variable][subdir] = "contrib"

projects[views][version] = "3.11"
projects[views][subdir] = "contrib"

projects[views_fieldsets][version] = "2.0"
projects[views_fieldsets][subdir] = "contrib"

; +++++ Themes +++++

; adaptivetheme
projects[adaptivetheme][type] = "theme"
projects[adaptivetheme][version] = "3.2"
projects[adaptivetheme][subdir] = "contrib"

