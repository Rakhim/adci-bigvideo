<?php
/**
 * @file
 * feature_bigvideo_views_preview.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_views_preview_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-preview-block'] = array(
    'cache' => -1,
    'css_class' => 'preview',
    'custom' => 0,
    'delta' => 'preview-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'content_aside',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => -9,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
