<?php
/**
 * @file
 * feature_bigvideo_block_footer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_bigvideo_block_footer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
