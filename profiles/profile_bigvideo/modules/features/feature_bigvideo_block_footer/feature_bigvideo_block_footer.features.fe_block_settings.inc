<?php
/**
 * @file
 * feature_bigvideo_block_footer.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_block_footer_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['boxes-footer'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'footer',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
