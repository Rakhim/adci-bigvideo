<?php
/**
 * @file
 * feature_bigvideo_block_continue.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feature_bigvideo_block_continue_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'continue';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Continue';
  $box->options = array(
    'body' => array(
      'value' => '<div class="site-name">
  <p >Your Video Site</p>
</div>
<div class="site-slogan">
  <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
</div>
<div class="continue-button-wrapper">
 <a  href="/#block-views-bigvideo-categories-block" class="continue-button">Continue</a>
</div>
<div class="continue-arrow">
<div></div>
</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'bigvideo-theme-continue',
  );
  $export['continue'] = $box;

  return $export;
}
