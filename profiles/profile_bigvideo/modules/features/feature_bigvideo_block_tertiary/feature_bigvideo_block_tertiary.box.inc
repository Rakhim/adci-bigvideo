<?php
/**
 * @file
 * feature_bigvideo_block_tertiary.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feature_bigvideo_block_tertiary_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_one';
  $box->plugin_key = 'simple';
  $box->title = 'Sed ut birdeme ';
  $box->description = 'Box One Tertiaruy';
  $box->options = array(
    'body' => array(
      'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_one'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_three';
  $box->plugin_key = 'simple';
  $box->title = 'Estenere urpaq';
  $box->description = 'Box Three Tertiary';
  $box->options = array(
    'body' => array(
      'value' => 'Ldo conseqorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commouat. ',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_three'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bigvideo_box_twio';
  $box->plugin_key = 'simple';
  $box->title = 'Estimate twice';
  $box->description = 'Box two tertiaryu';
  $box->options = array(
    'body' => array(
      'value' => 'Loreor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. m ipsum dol',
      'format' => 'filtered_html',
    ),
    'additional_classes' => '',
  );
  $export['bigvideo_box_twio'] = $box;

  return $export;
}
