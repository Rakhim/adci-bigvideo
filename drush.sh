#!/bin/bash

db_name=second
db_root=root
acc=admin

sudo find ! -name 'drush.sh' -delete

drush dl
mv drupal-7*/* ./
rm -r drupal-7*

drush -y si standard --account-name=$acc --account-pass=$acc --db-su=$db_root --db-su-pw=$db_root --site-name=name --db-url=mysql://$db_name:$db_name@localhost/$db_name
chmod 777 sites/default/files/
drush -y dis color comment dashboard shortcut overlay toolbar
drush -y pm-uninstall color comment dashboard shortcut overlay toolbar

path=sites/all/modules/contrib
mkdir $path
chmod 777 $path

drush en -y admin_menu, module_filter
#wget http://ftp.drupal.org/files/projects/videojs-7.x-3.0-alpha2.tar.gz -P $path/
#tar -xzf $path/videojs-7.x-3.0-alpha2.tar.gz -C $path

#base_path=sites/all/libraries

#wget http://www.videojs.com/downloads/video-js-4.12.11.zip -P $base_path/
#wget http://owlgraphic.com/owlcarousel/owl.carousel.zip -P $base_path/

#unzip $base_path/video-js-4.12.11.zip -d $base_path/
#unzip $base_path/owl.carousel.zip -d $base_path/

#rm $path/*.gz $base_path/*.zip

#drush dl admin_menu views module_filter devel token pathauto bigvideo advanced_help views_fieldsets features node_export owlcarousel
#drush -y en node_export_features exclude_node_title variable admin_menu views module_filter devel token pathauto bigvideo advanced_help views_fieldsets features videojs node_export owlcarousel views_ui
drush cc all

#drush adaptivetheme "profile sub-theme" profilesub
