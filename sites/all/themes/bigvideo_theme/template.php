<?php

  function bigvideo_theme_css_alter(&$css) {
    $val = theme_get_setting('bigvideo_theme_mode');
    $base_path = drupal_get_path("theme","bigvideo_theme");

    switch ($val) {
      case "dark": 
        unset($css[$base_path."/css/light.css"]);
      
        break;
      case "light":
        unset($css[$base_path."/css/dark.css"]);
      
        break; 
    }
  }
