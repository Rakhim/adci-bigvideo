<?php

/**
 * @file
 * Implimentation of hook_form_system_theme_settings_alter()
 *
 * To use replace "bigvideo_theme" with your themeName and uncomment by
 * deleting the comment line to enable.
 *
 * @param $form: Nested array of form elements that comprise the form.
 * @param $form_state: A keyed array containing the current state of the form.
 */
function bigvideo_theme_form_system_theme_settings_alter(&$form, &$form_state)  {
    $form['bigvideo_theme_settings']['bigvideo_theme_mode'] = array(
      '#type' => 'select',
      '#title' => t('Big Video theme '),
      '#options' => array(
        'dark' => t(' Dark'),
        'light' => t(' Light'),
      ),
      '#default_value' => theme_get_setting('bigvideo_theme_mode'),
      '#description' => t(' '),
  );
}
