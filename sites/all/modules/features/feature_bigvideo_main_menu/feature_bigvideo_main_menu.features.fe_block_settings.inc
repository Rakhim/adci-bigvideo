<?php
/**
 * @file
 * feature_bigvideo_main_menu.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_main_menu_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['system-main-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => -8,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
