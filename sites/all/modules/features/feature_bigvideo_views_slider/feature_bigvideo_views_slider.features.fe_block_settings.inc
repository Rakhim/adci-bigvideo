<?php
/**
 * @file
 * feature_bigvideo_views_slider.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_views_slider_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-slider-bigvideo_slider'] = array(
    'cache' => -1,
    'css_class' => 'owl-slider',
    'custom' => 0,
    'delta' => 'slider-bigvideo_slider',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'content_aside',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
