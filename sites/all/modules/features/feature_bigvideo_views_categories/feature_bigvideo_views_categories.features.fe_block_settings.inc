<?php
/**
 * @file
 * feature_bigvideo_views_categories.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_views_categories_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-bigvideo_categories-block'] = array(
    'cache' => -1,
    'css_class' => 'bigvideo-categories',
    'custom' => 0,
    'delta' => 'bigvideo_categories-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'content_aside',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
