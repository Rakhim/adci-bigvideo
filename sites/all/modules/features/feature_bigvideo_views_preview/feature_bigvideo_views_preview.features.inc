<?php
/**
 * @file
 * feature_bigvideo_views_preview.features.inc
 */

/**
 * Implements hook_views_api().
 */
function feature_bigvideo_views_preview_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function feature_bigvideo_views_preview_image_default_styles() {
  $styles = array();

  // Exported image style: vehicle.
  $styles['vehicle'] = array(
    'label' => 'Vehicle(270x300)',
    'effects' => array(
      1 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 270,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
