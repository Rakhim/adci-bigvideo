<?php
/**
 * @file
 * feature_bigvideo_block_footer.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feature_bigvideo_block_footer_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'footer';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Big Video theme footer';
  $box->options = array(
    'body' => array(
      'value' => '<a href="#">Home</a> |  <a href="#">Categories</a> | <a href="#">Blog</a> | <a href="#">Contact</a> 
<p>Drupal DEMO store. Copyright @ 2013 Advanced Themes by ADCI Solutions. All Rights Reserved. </p>',
      'format' => 'filtered_html',
    ),
    'additional_classes' => 'footer-copyright',
  );
  $export['footer'] = $box;

  return $export;
}
