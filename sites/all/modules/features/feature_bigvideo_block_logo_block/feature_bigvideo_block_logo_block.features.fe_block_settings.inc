<?php
/**
 * @file
 * feature_bigvideo_block_logo_block.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_block_logo_block_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['logo_block-logo'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'logo',
    'module' => 'logo_block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => -9,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
