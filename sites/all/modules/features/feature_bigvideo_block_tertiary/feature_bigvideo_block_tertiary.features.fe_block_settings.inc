<?php
/**
 * @file
 * feature_bigvideo_block_tertiary.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_block_tertiary_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['boxes-bigvideo_box_one'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'bigvideo_box_one',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'tertiary_content',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
    ),
    'title' => 'Sed ut birdeme ',
    'visibility' => 1,
  );

  $export['boxes-bigvideo_box_three'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'bigvideo_box_three',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'tertiary_content',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
    ),
    'title' => 'Estenere urpaq',
    'visibility' => 1,
  );

  $export['boxes-bigvideo_box_twio'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'bigvideo_box_twio',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'tertiary_content',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => 0,
      ),
    ),
    'title' => 'Estimate twice',
    'visibility' => 1,
  );

  return $export;
}
