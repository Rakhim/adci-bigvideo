<?php
/**
 * @file
 * feature_bigvideo_block_continue.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_bigvideo_block_continue_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
