<?php
/**
 * @file
 * feature_bigvideo_block_continue.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function feature_bigvideo_block_continue_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['boxes-continue'] = array(
    'cache' => -2,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'continue',
    'module' => 'boxes',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bigvideo_theme' => array(
        'region' => 'content_aside',
        'status' => 1,
        'theme' => 'bigvideo_theme',
        'weight' => -11,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
